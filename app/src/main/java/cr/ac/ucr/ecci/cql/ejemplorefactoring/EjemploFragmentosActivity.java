package cr.ac.ucr.ecci.cql.ejemplorefactoring;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class EjemploFragmentosActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejemplo_fragmentos);
    }
}